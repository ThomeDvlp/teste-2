import Vue from 'vue'
import axios from 'axios'

axios.defaults.baseURL = 'https://api.openweathermap.org/data/2.5'

Vue.use ({
  install(Vue) {
    Vue.prototype.$http = axios
  }
})